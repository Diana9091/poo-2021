package com.example.demo.dao;

import com.example.demo.dto.Usuario;

public interface Dao {
    Usuario obtenerUsuario(Usuario usuario);
    Usuario actualizarUsuario(Usuario usuario);
}
