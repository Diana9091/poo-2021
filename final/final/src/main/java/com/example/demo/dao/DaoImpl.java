package com.example.demo.dao;

import java.sql.*;

import com.example.demo.dto.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class DaoImpl implements Dao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void crearConexion(){
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(){
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @Override
    public Usuario obtenerUsuario(Usuario usuario) {
        Usuario respuesta = new Usuario();
        try {
            crearConexion();
            String SQL = " select usuario.id_usuario, usuario.nombres, usuario.apellidos, usuario.correo, usuario.administrador, usuario.clave\n" +
                    " from usuario\n" +
                    " where nombres = ? or apellidos = ? or correo = ?";
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setString(1,usuario.getNombres());
            sentencia.setString(2,usuario.getApellidos());
            sentencia.setString(3,usuario.getCorreo());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                respuesta.setId_usuario(resultado.getInt("id_usuario"));
                respuesta.setNombres(resultado.getString("nombres"));
                respuesta.setApellidos(resultado.getString("apellidos"));
                respuesta.setCorreo(resultado.getString("correo"));
                respuesta.setAdministrador(resultado.getString("administrador"));
                respuesta.setClave(resultado.getString("clave"));
                 
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return respuesta;

    }

    @Override
    public Usuario actualizarUsuario(Usuario usuario) {
        try {
            crearConexion();
            String SQL = " update usuario " +
                    " set nombres = ?, apellidos = ?, correo = ?, administrador = ?, clave = ? " +
                    " where id_usuario = ? ";
            PreparedStatement sentencia = conexion.prepareStatement(SQL);
            sentencia.setString(1,usuario.getNombres());
            sentencia.setString(2,usuario.getApellidos());
            sentencia.setString(3,usuario.getCorreo());
            sentencia.setString(4,usuario.getAdministrador());
            sentencia.setString(5,usuario.getClave());
            int filas = sentencia.executeUpdate();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;

    }
    
}
