package pregunta1;

public abstract class Moneda {
    private Double valor;
    private Double tasaCambio;
    
    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getTasaCambio() {
        return tasaCambio;
    }

    public void setTasaCambio(Double tasaCambio) {
        this.tasaCambio = tasaCambio;
    }

    public Double cambiar(Moneda moneda){
        Double valorNuevo=this.valor*this.tasaCambio;
        return valorNuevo;
    }
}
