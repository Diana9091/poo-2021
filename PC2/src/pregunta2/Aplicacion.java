package pregunta2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Aplicacion{

    Paciente[] paciente;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        Scanner entrada = new Scanner(System.in);
        
        System.out.print("Ingresa el numero de pacientes: ");
        int numero=entrada.nextInt();
        int i=0;

        while(i<numero){
            
            Paciente paciente[i] = new Paciente();

            System.out.println("Datos del paciente #"+i+": ");
            
            System.out.print("Ingresa el dni: ");
            paciente[i].dni=entrada.nextInt();

            System.out.print("Ingresa la edad: ");
            paciente[i].edad=entrada.nextInt();
            i++;

        }
        

    }

}