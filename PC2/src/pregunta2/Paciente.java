package pregunta2;

public class Paciente {

    int dni;
    int edad;
    String nombres;
    String apellido_materno;
    String apellido_paterno;
    boolean vacunado;
    
    public Paciente(String nombres, String apellido_materno, String apellido_paterno, boolean vacunado){
        this.nombres=nombres;
        this.apellido_materno=apellido_materno;
        this.apellido_materno=apellido_paterno;
        this.vacunado=vacunado;
    }

    public int getDni (){
        return dni;
    }
   
   public void setDni (int dni){
       if(this.dni>0&&this.dni<=100000000){
            this.dni=dni;
       }
       else{
           System.out.print("Error de insercion");
       }
   }

    public int getEdad (){
        return edad;
    }
    
    public void setEdad (int edad){
        if(this.edad>40&&this.edad<=125){
        this.edad=edad;
    }
        else{
            System.out.print("Error de insercion");
        }
    }
    
}
