package Pregunta1;

import java.util.ArrayList;
import java.util.List;

public class Aplicacion {
    public static void main(String[] args) {
        List<String> nombresAutores1 = new ArrayList<>();
        nombresAutores1.add("Garcia");
        nombresAutores1.add("Vargas");

        List<String> nombresAutores2 = new ArrayList<>();
        nombresAutores2.add("Neruda");
        nombresAutores2.add("Unamuno");
        nombresAutores2.add("Jimenez");

        List<String> titulosCapitulos1 = new ArrayList<>();
        titulosCapitulos1.add("Primer Capitulo");
        titulosCapitulos1.add("Segundo Capitulo");

        List<String> titulosCapitulos2 = new ArrayList<>();
        titulosCapitulos2.add("1er Capitulo");
        titulosCapitulos2.add("2do Capitulo");
        titulosCapitulos2.add("3er Capitulo");

        List<Libro> libros = new ArrayList<>();

        Libro libro1 = new Libro("Primer libro", nombresAutores1, 000001,"Libro de Garcia y Vargas", 
        "San Marcos", 2020, titulosCapitulos1, "Resumen Libro1");
        Libro libro2 = new Libro("Segundo libro", nombresAutores2, 000002,"Libro de Neruda, Unamuno y Jimenez", 
        "San Marcos", 2019, titulosCapitulos2, "Resumen Libro2");

        libros.add(libro1);
        libros.add(libro2);

        List<Libro> lista = new ArrayList<>();
        lista = Servicio.buscarLibro(libros,"libro");
        System.out.println(lista);

    }
}
