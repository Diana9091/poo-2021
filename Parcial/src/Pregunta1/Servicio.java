package Pregunta1;

import java.util.List;

public interface Servicio {
    List<Libro> buscarLibro(List<Libro> lista, String titulo);
}
