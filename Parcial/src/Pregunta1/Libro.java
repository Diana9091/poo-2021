package Pregunta1;

import java.util.List;

public class Libro {
    private String titulo;
    private List<String> nombresAutores;
    private Integer codigo;
    private String descripcion;
    private String editorial;
    private Integer anioPublicacion;
    private List<String> titulosCapitulos;
    private String resumen;

    public Libro(String titulo, List<String> nombresAutores, Integer codigo, String descripcion, String editorial,
            Integer anioPublicacion, List<String> titulosCapitulos, String resumen) {
        this.titulo = titulo;
        this.nombresAutores = nombresAutores;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.editorial = editorial;
        this.anioPublicacion = anioPublicacion;
        this.titulosCapitulos = titulosCapitulos;
        this.resumen = resumen;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<String> getNombresAutores() {
        return nombresAutores;
    }

    public void setNombresAutores(List<String> nombresAutores) {
        this.nombresAutores = nombresAutores;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public Integer getAnioPublicacion() {
        return anioPublicacion;
    }

    public void setAnioPublicacion(Integer anioPublicacion) {
        this.anioPublicacion = anioPublicacion;
    }

    public List<String> getTitulosCapitulos() {
        return titulosCapitulos;
    }

    public void setTitulosCapitulos(List<String> titulosCapitulos) {
        this.titulosCapitulos = titulosCapitulos;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    
}
