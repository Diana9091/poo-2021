package Pregunta1;

import java.util.ArrayList;
import java.util.List;

public class ServicioImpl implements Servicio{
    public List<Libro> buscarLibro(List<Libro> lista, String titulo){
        List<Libro> listaNueva = new ArrayList<>();
        for (Libro libro : lista) {
            if(libro.getTitulo().contains(titulo)){
                listaNueva.add(libro);
            }
        }

        return listaNueva;
    };
    
}
