package Pregunta2;
/*Cree una solución que permita registrar los datos de los postulantes
a un trabajo. El sistema debe registrar: número de documento de
identidad, nombres, apellidos, nivel educativo (primaria, secundaria o
superior), dirección, número telefónico, edad, nota de evaluación
(inicialmente 0).
Adicionalmente el sistema debe permitir la modificación de la nota
de evaluación.
Es obligatorio el uso de interfaces, manejo de excepciones y
enumeraciones (7 puntos).*/
public class Postulante implements Postulador{
    private Integer dni;
    private String nombres;
    private String apellidos;
    private NivelEducativo nivelEducativo;
    private String direccion;
    private Integer numeroTelefonico;
    private Integer edad;
    private Integer notaEvaluacion = 0;
    
    public Postulante(Integer dni, String nombres, String apellidos, NivelEducativo nivelEducativo, String direccion,
            Integer numeroTelefonico, Integer edad, Integer notaEvaluacion) {
        this.dni = dni;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.nivelEducativo = nivelEducativo;
        this.direccion = direccion;
        this.numeroTelefonico = numeroTelefonico;
        this.edad = edad;
        this.notaEvaluacion = notaEvaluacion;
    }
    public Integer getDni() {
        return dni;
    }
    public void setDni(Integer dni) {
        this.dni = dni;
    }
    public String getNombres() {
        return nombres;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public NivelEducativo getNivelEducativo() {
        return nivelEducativo;
    }
    public void setNivelEducativo(NivelEducativo nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public Integer getNumeroTelefonico() {
        return numeroTelefonico;
    }
    public void setNumeroTelefonico(Integer numeroTelefonico) {
        this.numeroTelefonico = numeroTelefonico;
    }
    public Integer getEdad() {
        return edad;
    }
    public void setEdad(Integer edad) {
        this.edad = edad;
    }
    public Integer getNotaEvaluacion() {
        return notaEvaluacion;
    }
    public void setNotaEvaluacion(Integer notaEvaluacion) {
        this.notaEvaluacion = notaEvaluacion;
    }

    void cambiarNota(Integer notaEvaluacion){
        
        return Integer notaEvaluacion;
    };

}
