package Pregunta2;

public enum NivelEducativo {
    PRIMARIA("Primaria"), SECUNDARIA("Secundaria"), SUPERIOR("Superior");
    private String nivel;

    NivelEducativo(String nivel) {
        this.nivel = nivel;
    }

    public String getNivel() {
        return nivel;
    }

}
