import  java.lang.Math;

public class Poligono {
    private Integer numeroLados;
    private Float longitudLado;
    
    public Integer getNumeroLados() {
        return numeroLados;
    }
    public void setNumeroLados(Integer numeroLados) {
        if(this.numeroLados>7||this.numeroLados<3){
            System.out.println("Error en numero de lados");
        }
        else{
            this.numeroLados = numeroLados;
        }
    }
    public Float getLongitudLado() {
        return longitudLado;
    }
    public void setLongitudLado(Float longitudLado) {
        if(this.longitudLado<=0){
            System.out.println("Error en longitud");
        }
        else{
            this.longitudLado = longitudLado;
        }
    }
    
    public Float calcularArea(Poligono poligono){
        Float area = 0;
        area = (0.5*calcularPerimetro(this.numeroLados, this.longitudLado))*(0.5*this.longitudLado/Math.tan(Math.PI/(this.numero_lados*2)));
        return area;
    }

    public static Float calcularPerimetro(Integer numeroLados,Float longitudLado){
        Float perimetro = numeroLados*longitudLado;
        return perimetro;
    }

    public static Float calcularNumDiagonales(Integer numeroLados,Float longitudLado){
        Float numDiagonales = 0;
        numDiagonales = numeroLados*(numeroLados-3)/2;
        return numDiagonales;
    }

}
