public class Aplicacion{
    public static void main(String[] args) {
        
        Calzado calzado1 = new Calzado();
        calzado1.setMarca("Puma");
        calzado1.setModelo("A");
        calzado1.setTipo("Infantil");
        calzado1.setHorma("Delgada");
        calzado1.setMaterial("Cuero");
        calzado1.setTemporada("Verano");
        calzado1.setPaisOrigen("Perú");

        Calzado calzado2 = new Calzado();
        calzado2.setMarca("Nike");
        calzado2.setModelo("B");
        calzado2.setTipo("Juvenil");
        calzado2.setHorma("Gruesa");
        calzado2.setMaterial("Sintético");
        calzado2.setTemporada("Invierno");
        calzado2.setPaisOrigen("EEUU");

        Calzado calzado3 = new Calzado();
        calzado3.setMarca("Puma");
        calzado3.setModelo("B");
        calzado3.setTipo("Adultos");
        calzado3.setHorma("Gruesa");
        calzado3.setMaterial("Sintético");
        calzado3.setTemporada("Otoño");
        calzado3.setPaisOrigen("EEUU");
        
        Venta venta1 = new Venta();
        venta1.setCalzado(calzado1);

        Venta venta2 = new Venta();
        venta2.setCalzado(calzado3);
    }
}