public class App {

    private Venta[] ventas;

    public static void main(String[] args) {

        App p = new App();
        p.ventas = new Venta[2];
        Producto[] productos1 = new Producto[3];
        p.ventas[0].setProductos(productos1);

        productos1[0].setNombre("Lapiz");
        productos1[0].setCodigo(123);
        productos1[0].setMontoUnidad(1.1);

        productos1[1].setNombre("Borrador");
        productos1[1].setCodigo(234);
        productos1[1].setMontoUnidad(0.5);

        productos1[2].setNombre("Lapicero");
        productos1[2].setCodigo(124);
        productos1[2].setMontoUnidad(1.5);

        Producto[] productos2 = new Producto[3];
        p.ventas[1].setProductos(productos2);

        productos1[0].setNombre("Toalla");
        productos1[0].setCodigo(235);
        productos1[0].setMontoUnidad(5.5);

        productos1[1].setNombre("Tajador");
        productos1[1].setCodigo(456);
        productos1[1].setMontoUnidad(0.5);

        productos1[2].setNombre("Regla");
        productos1[2].setCodigo(753);
        productos1[2].setMontoUnidad(0.4);
    
        p.totalVenta();

    }

    public void totalVenta(){
        
        double total=0d;

        for (int i = 0; i < this.ventas.length; i++) {
            
            for (int j = 0; j < this.ventas[i].getProductos().length; j++) {
                total = total + this.ventas[i].getProductos()[j].getMontoUnidad();
            }

            System.out.println("La venta total de Venta "+i+" es: "+total );
            total=0;

        }

    }

}