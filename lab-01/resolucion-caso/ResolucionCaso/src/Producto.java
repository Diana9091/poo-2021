public class Producto {
	private String nombre;
	private int codigo;
	private double montoUnidad;
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getCodigo() {
        return codigo;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public double getMontoUnidad() {
        return montoUnidad;
    }
    public void setMontoUnidad(double montoUnidad) {
        this.montoUnidad = montoUnidad;
    }
    
}
