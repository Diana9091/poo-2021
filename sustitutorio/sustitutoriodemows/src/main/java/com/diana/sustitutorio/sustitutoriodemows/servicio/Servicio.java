package com.diana.sustitutorio.sustitutoriodemows.servicio;

import com.diana.sustitutorio.sustitutoriodemows.dto.model.Curso;
import com.diana.sustitutorio.sustitutoriodemows.dto.model.Estudiante;

import java.util.List;

public interface Servicio {
    public Estudiante autenticarEstudiante(Estudiante estudiante);
    public List<Curso> obtenerCurso(Curso curso);
}

