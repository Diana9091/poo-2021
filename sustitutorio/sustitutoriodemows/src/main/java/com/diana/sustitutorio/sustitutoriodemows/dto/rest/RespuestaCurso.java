package com.diana.sustitutorio.sustitutoriodemows.dto.rest;
import com.diana.sustitutorio.sustitutoriodemows.dto.model.Curso;
import lombok.Data;

import java.util.List;

@Data
public class RespuestaCurso {
    private List<Curso> lista;

    public RespuestaCurso(List<Curso> lista) {
        this.lista = lista;
    }

    public RespuestaCurso() {
    }

    
}
