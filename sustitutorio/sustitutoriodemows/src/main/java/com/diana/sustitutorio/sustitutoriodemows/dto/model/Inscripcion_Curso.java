package com.diana.sustitutorio.sustitutoriodemows.dto.model;

import lombok.Data;

@Data
public class Inscripcion_Curso {
    private Integer id_curso;
    private Integer id_estudiante;
    private String fecha_inscripcion;
    
    public Inscripcion_Curso(Integer id_curso, Integer id_estudiante, String fecha_inscripcion) {
        this.id_curso = id_curso;
        this.id_estudiante = id_estudiante;
        this.fecha_inscripcion = fecha_inscripcion;
    }

    

}
