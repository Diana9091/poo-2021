package com.diana.sustitutorio.sustitutoriodemows.servicio;

import com.diana.sustitutorio.sustitutoriodemows.dao.Dao;
import com.diana.sustitutorio.sustitutoriodemows.dto.model.Curso;
import com.diana.sustitutorio.sustitutoriodemows.dto.model.Estudiante;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;
    public Estudiante autenticarEstudiante(Estudiante estudiante) {
        return dao.autenticarEstudiante(estudiante);
    }

    public List<Curso> obtenerCurso(Curso curso){
        return dao.obtenerCurso(curso);
    }
}

