package com.diana.sustitutorio.sustitutoriodemows.dto.model;

import lombok.Data;

@Data
public class Estudiante {
    private Integer id_estudiante;
    private String nombres;
    private String apellidos;
    private String correo;
    private String clave;
    
    public Estudiante(Integer id_estudiante, String nombres, String apellidos, String correo, String clave) {
        this.id_estudiante = id_estudiante;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.clave = clave;
    }

    public Estudiante() {
    }

    
    
}
