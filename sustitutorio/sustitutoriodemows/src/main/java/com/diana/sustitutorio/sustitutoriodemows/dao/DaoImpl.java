package com.diana.sustitutorio.sustitutoriodemows.dao;

import com.diana.sustitutorio.sustitutoriodemows.dto.model.Curso;
import com.diana.sustitutorio.sustitutoriodemows.dto.model.Estudiante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Estudiante autenticarEstudiante(Estudiante estudiante) {
        String SQL = " SELECT id_estudiante,nombres,apellidos,correo,\n" +
                " FROM estudiante\n" +
                " WHERE correo = ? AND clave = ? ";

        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setString(1,estudiante.getCorreo());
            sentencia.setString(2,estudiante.getClave());
            ResultSet resultado = sentencia.executeQuery();
            estudiante = new Estudiante();
            while(resultado.next()){
                estudiante = extraerEstudiante(resultado);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return estudiante;
    }

    private Estudiante extraerEstudiante(ResultSet resultado) throws SQLException {
        Estudiante retorno = new Estudiante(
            resultado.getInt("id_estudiante"),
            resultado.getString("nombres"),
            resultado.getString("apellidos"),
            resultado.getString("correo"),
                null
        );
        return retorno;
    }

    public List<Curso> obtenerCurso(Curso curso){
        List<Curso> cursos = new ArrayList<>();
        String SQL = "SELECT c.id_curso, c.precio, c.fecha_inicio, c.fecha_final,\n" +
                "FROM curso c \n" +
                "JOIN Inscripcion_curso i on c.id_curso = i.id_curso\n"+
                "WHERE c.id_curso = ?";

        try {
            obtenerConexion();
            PreparedStatement sentencia = this.conexion.prepareStatement(SQL);
            sentencia.setInt(1,curso.getId_curso());
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                Curso c = extraerCurso(resultado);
                cursos.add(c);
            }
            cerrarConexion(resultado,sentencia);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return cursos;
    }

    private Curso extraerCurso(ResultSet resultado) throws SQLException {
        Curso curso = new Curso(
                resultado.getInt("id_curso"),
                resultado.getString("nombre"),
                resultado.getFloat("precio"),
                resultado.getString("fecha_inicio"),
                resultado.getString("fecha_fin")
        );
        return curso;
    }


}
