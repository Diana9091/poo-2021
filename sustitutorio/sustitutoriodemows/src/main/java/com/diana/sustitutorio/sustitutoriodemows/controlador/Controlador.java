package com.diana.sustitutorio.sustitutoriodemows.controlador;

import com.diana.sustitutorio.sustitutoriodemows.dto.model.Estudiante;
import com.diana.sustitutorio.sustitutoriodemows.dto.model.Curso;
import com.diana.sustitutorio.sustitutoriodemows.dto.rest.RespuestaCurso;
import com.diana.sustitutorio.sustitutoriodemows.servicio.Servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
    @Autowired
    private Servicio servicio;
    @RequestMapping(
            value = "autenticar-estudiante",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody Estudiante autenticarEstudiante(@RequestBody Estudiante estudiante){
        if(estudiante == null || estudiante.getId_estudiante() != null
                || estudiante.getNombres() != null
                || estudiante.getApellidos() != null
        ){
            return new Estudiante();
        }
        else{
            return servicio.autenticarEstudiante(estudiante);
        }
    }

    @RequestMapping(
            value = "obtener-curso",method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody RespuestaCurso buscarProductos(@RequestBody Curso curso){
        RespuestaCurso obtenerCurso = new RespuestaCurso();
        RespuestaCurso.setLista(servicio.obtenerCurso(curso));
        return obtenerCurso;
    }
}

