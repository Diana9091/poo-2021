CREATE TABLE estudiante(
	id_estudiante NUMERIC(9) PRIMARY KEY,
	nombres VARCHAR(200),
	apellidos VARCHAR(200),
	correo VARCHAR(200) UNIQUE,
	clave VARCHAR(200)
);

INSERT INTO estudiante(id_estudiante, nombres, apellidos, correo, clave)
VALUES('1','Diana','Montoya','diana@gmail.com','JavaOcho');

INSERT INTO estudiante(id_estudiante, nombres, apellidos, correo, clave)
VALUES('2','Carolina','Namoc','carolina@gmail.com','JavaOcho');

CREATE TABLE Inscripcion_curso(
	id_curso NUMERIC(9),
	id_estudiante NUMERIC(9),
	precio NUMERIC(9,2),
	fecha_inscripcion VARCHAR(10)
);

CREATE TABLE curso(
	id_curso NUMERIC(9) PRIMARY KEY,
	nombre VARCHAR(200),
	precio NUMERIC(9,2),
	fecha_inicio VARCHAR(10),
	fecha_fin VARCHAR(10)
);