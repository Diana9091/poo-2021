import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { MostrarcursosestudianteComponent } from './mostrarcursosestudiante/mostrarcursosestudiante.component';
import { MostrarcursostotalComponent } from './mostrarcursostotal/mostrarcursostotal.component';

@NgModule({
  declarations: [
    AppComponent,
    MostrarcursosestudianteComponent,
    MostrarcursostotalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
