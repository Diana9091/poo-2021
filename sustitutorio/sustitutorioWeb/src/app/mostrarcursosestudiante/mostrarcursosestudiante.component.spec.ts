import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarcursosestudianteComponent } from './mostrarcursosestudiante.component';

describe('MostrarcursosestudianteComponent', () => {
  let component: MostrarcursosestudianteComponent;
  let fixture: ComponentFixture<MostrarcursosestudianteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarcursosestudianteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarcursosestudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
