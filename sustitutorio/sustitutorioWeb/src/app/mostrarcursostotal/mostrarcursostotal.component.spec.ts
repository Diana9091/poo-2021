import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarcursostotalComponent } from './mostrarcursostotal.component';

describe('MostrarcursostotalComponent', () => {
  let component: MostrarcursostotalComponent;
  let fixture: ComponentFixture<MostrarcursostotalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarcursostotalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarcursostotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
