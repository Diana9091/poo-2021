export interface Estudiante{
    id_usuario: number;
    nombres: String;
    apellidos: String;
    correo: String;
    administrador: String;
    clave: String;
}

export interface Curso{
    id_usuario: number;
    nombres: String;
    apellidos: String;
    correo: String;
    administrador: String;
    clave: String;
}
export interface Inscripcion_curso{
    id_usuario: number;
    nombres: String;
    apellidos: String;
    correo: String;
    administrador: String;
    clave: String;
}

export interface RespuestaCursos{
    lista: Curso[];
}


